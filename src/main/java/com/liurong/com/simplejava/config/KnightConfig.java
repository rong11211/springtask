package com.liurong.com.simplejava.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.liurong.com.simplejava.BraveKnight;
import com.liurong.com.simplejava.Knight;
import com.liurong.com.simplejava.Quest;
import com.liurong.com.simplejava.SlayDragonQuest;

/**
 * ����java����bean
 * @author liu.r
 *
 */
@Configuration
public class KnightConfig {
	@Bean
	public Knight knight() {
		return new BraveKnight(quest());
	}

	@Bean
	public Quest quest() {
		return new SlayDragonQuest(System.out);
	}
}
