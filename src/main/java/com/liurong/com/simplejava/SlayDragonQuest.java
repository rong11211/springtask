package com.liurong.com.simplejava;

import java.io.PrintStream;

/**
 * �����̽������
 * @author li.r
 *
 */
public class SlayDragonQuest implements Quest {
	
	private PrintStream stream;
	
	public SlayDragonQuest(PrintStream stream) {
		super();
		this.stream = stream;
	}

	@Override
	public void embark() {
		stream.print("embarking on quest to slay the dragon");
	}
}
