package com.liurong.com.scope.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan(basePackages="com.liurong.com")
@ImportResource("classpath:applicationContext.xml")
public class ScopeConfig {
	
}
