package com.liurong.com.scope;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 作用域：
 * Prototype(原型):每次注入或者通过Spring应用上下文获取的时候，都会创建一个新的bean实例
 * Singleton(单例):在整个应用中，只创建bean的一个实例
 * @author liu.r
 *
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CountClass {
	private int a = 0;
	
	public int count() {
		return a++;
	}
}
