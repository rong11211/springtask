package com.liurong.com.factorybean;

import org.springframework.beans.factory.FactoryBean;

public class StudentBean implements FactoryBean<Student> {
	private Student student;
	
	public StudentBean() {
		student = new Student();
		student.setAge(24);
		student.setName("liurong");
	}
	
	
	public Student getObject() throws Exception {
		return this.student;
	}

	public Class<?> getObjectType() {
		return Student.class;
	}

	public boolean isSingleton() {
		return true;
	}

}
