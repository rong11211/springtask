package com.liurong.com.cache.ehcache;

import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.ehcache.EhCacheCacheManager;

/**
 * ehcache缓存类
 * 
 * @author liu.r
 *
 */
public class EhCache {

	private EhCacheCacheManager ehCacheCacheManager;

	public EhCacheCacheManager getEhCacheCacheManager() {
		return ehCacheCacheManager;
	}

	public void setEhCacheCacheManager(EhCacheCacheManager ehCacheCacheManager) {
		this.ehCacheCacheManager = ehCacheCacheManager;
	}

	public void put(String cacheName, Object key, Object value) {
		ehCacheCacheManager.getCache(cacheName).put(key, value);
	}

	public Object get(String cacheName, Object key) {
		ValueWrapper valueWrapper = ehCacheCacheManager.getCache(cacheName)
				.get(key);
		if (valueWrapper == null) {
			return null;
		}
		return valueWrapper.get();
	}
}
