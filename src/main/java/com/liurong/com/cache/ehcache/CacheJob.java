package com.liurong.com.cache.ehcache;

import org.springframework.beans.factory.annotation.Autowired;

public class CacheJob extends Thread {
	@Autowired
	private EhCache ehCache;
	
	public CacheJob() {
	}

	public CacheJob(EhCache ehCache) {
		this.ehCache = ehCache;
	}
	
	public void init() {
		ehCache.put("departCache","liurong", "liurong");
		ehCache.put("sencondCache","liurong", "lr");
		new CacheJob(ehCache).start();
		new CacheJob(ehCache).start();
	}

	@Override
	public void run() {
		while (true) {
			System.out.println("缓存数据为：" + ehCache.get("departCache", "liurong"));
			System.out.println("缓存数据为：" + ehCache.get("sencondCache", "liurong"));
			try {
				Thread.sleep(2000l);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
