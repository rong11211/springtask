package com.liurong.com.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class PropertiesTest {
	@Value("${abc}") 
	// @Value("#{T(System).currentTimeMillis()}")
	// @Value("#{systemProperties['abc']}")
	private String message;

	/*public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}*/

	public PropertiesTest() {
	}
	
	public void show() {
		System.out.println("properties注入  ：" + message);
	} 
}
