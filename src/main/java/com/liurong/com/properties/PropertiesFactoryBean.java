package com.liurong.com.properties;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

/**
 * FactoryBean
 * InitializingBean
 * @author liu.r
 *
 */
public class PropertiesFactoryBean implements FactoryBean<Properties>, InitializingBean {
	private Properties properties = new Properties();
	private Resource[] locations;

	public PropertiesFactoryBean(Resource[] locations) {
		this.locations = locations;
		//this.createProperties();
	}

	private void createProperties() {
		if (locations != null && locations.length > 0) {
			for (Resource location : locations) {
				try {
					PropertiesLoaderUtils.fillProperties(properties, location);
				} catch (IOException e) {
					System.out.println("加载配置出了异常！" + e);
				}
			}
		} else {
			System.out.println("需要有配置中心！");
		}
	}

	@Override
	public Properties getObject() throws Exception {
		return this.properties;
	}

	@Override
	public Class<?> getObjectType() {
		return Properties.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.createProperties();
	}
}

/*public class PropertiesFactoryBean implements FactoryBean<Properties> {
	private Properties properties = new Properties();
	private String[] locations;

	public PropertiesFactoryBean(String[] locations) {
		this.locations = locations;
		this.createProperties();
	}

	private void createProperties() {
		if (locations != null && locations.length > 0) {
			Resource resource = null;
			for (String location : locations) {
				try {
					resource = new FileSystemResource(location);
					PropertiesLoaderUtils.fillProperties(properties, resource);
				} catch (IOException e) {
					System.out.println("加载配置出了异常！" + e);
				}
			}
		} else {
			System.out.println("需要有配置中心！");
		}
	}

	@Override
	public Properties getObject() throws Exception {
		return this.properties;
	}

	@Override
	public Class<?> getObjectType() {
		return Properties.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
*/