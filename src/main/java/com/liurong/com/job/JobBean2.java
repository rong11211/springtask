package com.liurong.com.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.liurong.com.scope.CountClass;

@Lazy(value=false)
@Component
public class JobBean2 {
	//@Scheduled(cron = "*/5 * * * * ?")
	/*public static void singing() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println(sdf.format(date));
	}*/
	@Autowired
	private CountClass countClass;
	
	//@Scheduled(cron = "*/3 * * * * ?")
	public void singing() {
		System.out.println("job2调用的次数为：" + countClass.count());
	}
}
