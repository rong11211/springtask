package com.liurong.com.di.scan.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="com.liurong.com.di.scan")
public class AnimalsConfig {

}
