package springtest.simplejava;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.liurong.com.simplejava.Knight;
import com.liurong.com.simplejava.config.KnightConfig;

public class KnightMain {
	public static void main(String[] args) throws IOException {
		/**
		 * 使用应用上下文：
		 * 	AnnotationConfigApplicationContext:从一个或者多个基于java配置类加载spring应用上下文
		 * 	AnnotationConfigWebApplicationContext：从一个或者多个基于java配置类加载spring web应用上下文
		 * 	ClassPathXmlApplicationContext:从类路径下的一个或者多个XML配置文件中加载上下文
		 * 	FileSystemXmlApplicationContext:从文件系统下的一个或多个XML配置文件中加载上下文定义
		 * 	XMLWebApplicationContext:从web应用下的一个或多个XML配置文件中加载上下文定义
		 */
		ApplicationContext beans = new AnnotationConfigApplicationContext(
				KnightConfig.class);
		Knight knight = beans.getBean(Knight.class);
		knight.embarkOnQuest();
	}
}
