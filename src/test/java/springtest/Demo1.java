package springtest;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.liurong.com.factorybean.StudentBean;

public class Demo1 extends BaseTest {
	
	@Test
	public void fun1() throws Exception {
		ApplicationContext factory = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// getBean("studentBean")相当于调getObject方法
		System.out.println(factory.getBean("studentBean"));
		// getBean("&studentBean");相当于带动getObjectType方法
		System.out.println(factory.getBean("&studentBean"));
	}
}
