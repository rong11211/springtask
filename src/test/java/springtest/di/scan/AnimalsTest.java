package springtest.di.scan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.liurong.com.di.scan.Animals;
import com.liurong.com.di.scan.config.AnimalsConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AnimalsConfig.class)
public class AnimalsTest {
	@Autowired
	private Animals cat;
	@Autowired
	private Animals dog;
	
	
	@Test
	public void fun() {
		cat.eat();
		dog.eat();
	}
}
