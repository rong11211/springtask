package springtest;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.liurong.com.job.JobBean;

public class Demo2 extends BaseTest {
	@Autowired
	private JobBean jobBean;
	
	public void fun2() throws IOException {  
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");  
		JobBean jobBean = (JobBean)context.getBean("jobBean"); 
		System.in.read(); // 为保证服务一直开着，利用输入流的阻塞来模拟
	}
	
	// 注解注入
	@Test
	public void fun1() throws IOException {
		System.in.read();
	}
}
